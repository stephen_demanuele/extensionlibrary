﻿using System;
using PrimitiveExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class IsPalindromeTest
    {
        [TestMethod]
        public void TestInt()
        {
            Assert.IsTrue(
                987654321123456789.IsPalindrome() &&            
                !123.IsPalindrome() &&
                 323.IsPalindrome() &&
                 3223.IsPalindrome() &&
                 34543.IsPalindrome());
        }

        [TestMethod]
        public void TestLong()
        {
            Assert.IsTrue(
                !1234567890.IsPalindrome() &&
     987654321123456789.IsPalindrome() &&
     3443.IsPalindrome());
        }

        [TestMethod]
        public void TestULong()
        {
            Assert.IsTrue(
                !1234567890.IsPalindrome() &&
     9876543211123456789.IsPalindrome() &&
     3443.IsPalindrome());
        }

        [TestMethod]
        public void TestString()
        {
            Assert.IsTrue(
                "a".IsPalindrome() &&
                "aba".IsPalindrome() &&
                "abba".IsPalindrome());
        }
    }
}
