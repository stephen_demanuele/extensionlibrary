﻿using System;
using PrimitiveExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class PrimeNumberDetectionTest
    {
        [TestMethod]
        public void TestInt()
        {
            Assert.IsTrue(
                !4.IsPrime() &&
                17.IsPrime());
        }

        [TestMethod]
        public void TestLong()
        {
            Assert.IsTrue(
                !((long)4).IsPrime() &&
                 ((long)17).IsPrime());   
        }
    }
}
