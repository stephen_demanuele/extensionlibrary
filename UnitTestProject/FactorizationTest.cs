﻿using System;
using PrimitiveExtensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace UnitTestProject
{
    [TestClass]
    public class FactorizationTest
    {
        [TestMethod]
        public void TestInt()
        {
            int eight = 8;
            List<int> factors1 = eight.getFactors();

            int seventeen = 17;
            List<int> factors2 = seventeen.getFactors();

            Assert.IsTrue(factors1.Count == 4 &&
                factors1.Contains(1) &&
                factors1.Contains(8) &&
                factors1.Contains(2) &&
                factors1.Contains(4),
                "factors of 8, wrong!");

            Assert.IsTrue(factors2.Count == 2 &&
    factors2.Contains(1) &&
    factors2.Contains(17),
    "factors of 17, wrong!");
        }

        [TestMethod]
        public void TestLong()
        {
            long eight = 8;
            List<int> factors1 = eight.getFactors();

            int seventeen = 17;
            List<int> factors2 = seventeen.getFactors();

            Assert.IsTrue(factors1.Count == 4 &&
                factors1.Contains(1) &&
                factors1.Contains(8) &&
                factors1.Contains(2) &&
                factors1.Contains(4),
                "factors of 8, wrong!");

            Assert.IsTrue(factors2.Count == 2 &&
    factors2.Contains(1) &&
    factors2.Contains(17),
    "factors of 17, wrong!");
        }
    }
}
