﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimitiveExtensions
{
    public static class Extensions
    {

        //noun pal·in·drome \ˈpa-lən-ˌdrōm\
        //: a word, phrase, or number that reads the same backward or forward

        #region palindrome
        
        public static bool IsPalindrome(this short n)
        {
            return n.ToString().IsPalindrome();
        }

        public static bool IsPalindrome(this int n)
        {
            return n.ToString().IsPalindrome();
        }

        public static bool IsPalindrome(this long n)
        {
            return n.ToString().IsPalindrome();
        }

        public static bool IsPalindrome(this ulong n)
        {
            return n.ToString().IsPalindrome();
        }

        public static bool IsPalindrome(this string n)
        {
            int front = 0, back = n.Length - 1;
            while (front <= back)
            {
                if (n[front] != n[back]) return false;
                else
                {
                    front++; back--;
                }
            }
            return true;
        }
        
        #endregion

        #region find factors

        public static List<int> getFactors(this long number)
        {
            List<int> divisors = new List<int>();
            int iterateTill = (int)Math.Ceiling(Math.Sqrt(number));
            for (int i = 1; i < iterateTill; i++)
            {
                long rem = 0;
                Math.DivRem(number, i, out rem);
                if (rem != 0) continue;
                else
                {
                    divisors.AddRange(new List<int>(){ i, (int)(number/i) });
                }
            }
            return divisors;
        }

        public static List<int> getFactors(this int number)
        {
            List<int> divisors = new List<int>();
            int iterateTill = (int)Math.Ceiling(Math.Sqrt(number));
            for (int i = 1; i < iterateTill; i++)
            {
                long rem = 0;
                Math.DivRem(number, i, out rem);
                if (rem != 0) continue;
                else
                {
                    divisors.AddRange(new List<int>() { i, (int)(number / i) });
                }
            }
            return divisors;
        }

        #endregion

        #region detect a prime number

        public static bool IsPrime(this int x)
        {
            if (x >=1 && x <= 3) return true;
            int iterateTill = (int)Math.Ceiling(Math.Sqrt(x));
            for (int i = 2; i <= iterateTill; i++)
            {
                long rem = 0;
                Math.DivRem(x, i, out rem);
                if (rem == 0) return false;
            }
            return true;
        }

        public static bool IsPrime(this long x)
        {
            if (x >= 1 && x <= 3) return true;
            int iterateTill = (int)Math.Ceiling(Math.Sqrt(x));
            for (int i = 2; i <= iterateTill; i++)
            {
                long rem = 0;
                Math.DivRem(x, i, out rem);
                if (rem == 0) return false;
            }
            return true;
        }

        #endregion
    }
}
